# -*- coding: UTF-8 -*-
# Copyright 2020 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""General demo data for Lino Mentori.

"""

import datetime

from django.conf import settings
from lino.api import dd, rt, _
from lino.utils.mti import insert_child
from lino.utils.cycler import Cycler


def objects():
    """This will be called by the :ref:`dpy` deserializer during
    :cmd:`pm prep` and must yield a list of object instances to
    be saved.

    """
    Course = rt.models.courses.Course
    Enrolment = rt.models.courses.Enrolment
    Line = rt.models.courses.Line
    Topic = rt.models.courses.Topic
    ActivityLayouts = rt.models.courses.ActivityLayouts
    Person = rt.models.contacts.Person
    Company = rt.models.contacts.Company
    Pupil = rt.models.contacts.Person
    Teacher = rt.models.contacts.Person
    # Pupil = rt.models.contacts.Pupil
    # Teacher = rt.models.contacts.Teacher
    # for obj in Person.objects.all():
    #     if obj.pk % 5:
    #         insert_child(obj, Pupil)
    #     else:
    #         insert_child(obj, Teacher)
    TEACHERS = Cycler(Teacher.objects.all())
    CENTERS = Cycler(Company.objects.all())
    PUPILS = Cycler(reversed(Pupil.objects.all()))

    gm = Topic(**dd.str2kw('name', _("General medicine")))
    yield gm
    ph = Topic(**dd.str2kw('name', _("Pharmacology")))
    yield ph
    ch = Topic(**dd.str2kw('name', _("Chirugy")))
    yield ch

    # for n in ["A center", "B center", "C center"]:
    #     yield Company(name=n)
    #

    a = CENTERS.pop()

    yield Line(**dd.str2kw('name',
                           _("Thessaloniki center / instructor 1)"),
                           course_area=ActivityLayouts.internships,
                           topic=gm,
                           company=a))
    yield Line(**dd.str2kw('name',
                           _("Thessaloniki center / instructor 2)"),
                           course_area=ActivityLayouts.internships,
                           topic=ph,
                           company=a))
    yield Line(**dd.str2kw('name',
                           _("Thermi center / instructor 3)"),
                           course_area=ActivityLayouts.internships,
                           topic=gm,
                           company=CENTERS.pop()))
    yield Line(**dd.str2kw('name',
                           _("Seminar 1"),
                           course_area=ActivityLayouts.seminars,
                           topic=gm,
                           company=CENTERS.pop()))
    yield Line(**dd.str2kw(
        'name', _("Class"), course_area=ActivityLayouts.default, topic=gm))

    CLASSES = Cycler(Line.objects.filter(course_area=ActivityLayouts.default))
    SEMINARS = Cycler(
        Line.objects.filter(course_area=ActivityLayouts.seminars))
    INTERNSHIPS = Cycler(
        Line.objects.filter(course_area=ActivityLayouts.internships))

    demo_year = settings.SITE.the_demo_date.year

    for year in range(demo_year - 4, demo_year):
        d = datetime.date(year, 9, 1)
        for i in range(2):
            obj = Course(name="{}-{}".format(year, i + 1),
                         line=CLASSES.pop(),
                         start_date=d)
            yield obj
            for j in range(20):
                yield Enrolment(pupil=PUPILS.pop(), course=obj)
        for i in range(5):
            teacher = TEACHERS.pop()
            obj = Course(name="{} {}".format(teacher.last_name, year),
                         line=INTERNSHIPS.pop(),
                         teacher=teacher,
                         start_date=d)
            yield obj
            for j in range(5):
                yield Enrolment(pupil=PUPILS.pop(), course=obj)

        for i in range(3):
            obj = Course(name="{}-{}".format(year, i + 1),
                         line=SEMINARS.pop(),
                         teacher=TEACHERS.pop(),
                         start_date=d)
            yield obj
            for j in range(3):
                yield Enrolment(pupil=TEACHERS.pop(), course=obj)
