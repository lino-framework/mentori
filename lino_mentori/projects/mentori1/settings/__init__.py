# -*- coding: UTF-8 -*-
# Copyright 2016-2020 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino_mentori.lib.mentori.settings import *


class Site(Site):

    languages = "en fr"


USE_TZ = True
TIME_ZONE = 'UTC'
