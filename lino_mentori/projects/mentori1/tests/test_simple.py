# -*- coding: utf-8 -*-
# Copyright 2011-2020 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
You can run only these tests by issuing::

  $ go mentori
  $ cd lino_mentori/projects/mentori1
  $ python manage.py test tests.test_simple

"""

from django.core.exceptions import ValidationError
from lino.utils.djangotest import RemoteAuthTestCase
from lino.api import rt


def create(m, **kwargs):
    obj = m(**kwargs)
    obj.full_clean()
    obj.save()
    return obj


class SimpleTests(RemoteAuthTestCase):
    maxDiff = None

    def test01(self):
        User = rt.models.users.User
        UserTypes = rt.models.users.UserTypes
        Person = rt.models.contacts.Person

        robin = create(User,
                       username='robin',
                       user_type=UserTypes.admin,
                       language="en")

        foo = create(Person, first_name='Joe', last_name="Doe")
