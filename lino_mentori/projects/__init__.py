# -*- coding: UTF-8 -*-
# Copyright 2020 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""Projects included with Lino .

.. autosummary::
   :toctree:

   1

"""
