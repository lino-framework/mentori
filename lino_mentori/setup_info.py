# -*- coding: UTF-8 -*-
# Copyright 2016-2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
# python setup.py test -s tests/test_packages.py

SETUP_INFO = dict(
    name='lino-mentori',
    version='0.1.0',
    install_requires=['lino-xl'],
    description=
    ("A Lino Django application for managing internships, mentors and students"
     ),
    author='Rumma & Ko Ltd',
    author_email='info@lino-framework.org',
    url="https://gitlab.com/lino-framework/mentori",
    license_files=['COPYING'],
    test_suite='tests')

SETUP_INFO.update(long_description="""

A Lino application for managing the students of a school and their internships
and mentors in different organizations.

Project homepage: https://gitlab.com/lino-framework/mentori

Documentation: https://lino-framework.gitlab.io/mentori/

""")

SETUP_INFO.update(classifiers="""
Programming Language :: Python
Programming Language :: Python :: 3
Development Status :: 4 - Beta
Environment :: Web Environment
Framework :: Django
Intended Audience :: Developers
Intended Audience :: System Administrators
Intended Audience :: Information Technology
Intended Audience :: Customer Service
License :: OSI Approved :: GNU Affero General Public License v3
Operating System :: OS Independent
Topic :: Software Development :: Bug Tracking
""".format(**SETUP_INFO).strip().splitlines())
SETUP_INFO.update(packages=[
    'lino_mentori',
    'lino_mentori.lib',
    'lino_mentori.lib.mentori',
    'lino_mentori.lib.contacts',
    'lino_mentori.lib.contacts.fixtures',
    'lino_mentori.lib.courses',
    'lino_mentori.lib.users',
    'lino_mentori.lib.users.fixtures',
    'lino_mentori.projects',
    'lino_mentori.projects.mentori1',
    'lino_mentori.projects.mentori1.tests',
    'lino_mentori.projects.mentori1.settings',
    'lino_mentori.projects.mentori1.settings.fixtures',
])

SETUP_INFO.update(package_data=dict())
