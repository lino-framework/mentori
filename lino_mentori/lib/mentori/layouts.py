# -*- coding: UTF-8 -*-
# Copyright 2017-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

"""The default :attr:`custom_layouts_module
<lino.core.site.Site.custom_layouts_module>` for Lino Avanti.

"""

from lino.api import dd, rt, _
from lino.core import constants

rt.models.system.SiteConfigs.set_detail_layout("""
site_company next_partner_id:10
default_build_method simulate_today
""",
                                               size=(60, 'auto'))

rt.models.countries.Places.detail_layout = """
name country
type parent zip_code id
PlacesByPlace contacts.PartnersByCity
"""

rt.models.courses.AllEnrolments.column_names = \
'id #request_date #start_date #end_date #user course \
pupil__birth_date pupil__age pupil__country pupil__city \
pupil__gender state'

dd.update_field(rt.models.contacts.Partner,
                'language',
                verbose_name=_("Contact language"))

# rt.models.cv.LanguageKnowledgesByPerson.display_mode = ((None, constants.DISPLAY_MODE_TABLE), )
