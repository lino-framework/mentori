# -*- coding: UTF-8 -*-
# Copyright 2017-2020 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.projects.std.settings import *
from lino.api.ad import _


class Site(Site):

    verbose_name = "Lino Mentori"
    # version = '2017.1.0'
    url = "https://gitlab.com/lino-framework/mentori"
    demo_fixtures = [
        'std',
        'few_languages',
        'compass',
        # 'all_countries', 'all_languages',
        'demo',
        'demo2',
        'checkdata'
    ]
    user_types_module = 'lino_mentori.lib.mentori.user_types'
    workflows_module = 'lino_mentori.lib.mentori.workflows'
    custom_layouts_module = 'lino_mentori.lib.mentori.layouts'
    migration_class = 'lino_mentori.lib.mentori.migrate.Migrator'

    # project_model = 'contacts.Person'
    textfield_format = 'plain'
    # textfield_format = 'html'
    # use_silk_icons = False
    default_build_method = "appypdf"

    webdav_protocol = 'webdav'
    default_ui = 'lino_react.react'

    auto_configure_logger_names = 'lino lino_xl lino_mentori'

    def get_installed_plugins(self):
        """Implements :meth:`lino.core.site.Site.get_installed_plugins`.

        """
        yield super(Site, self).get_installed_plugins()
        yield 'lino_mentori.lib.users'
        yield 'lino_xl.lib.countries'
        # yield 'lino_xl.lib.topics'
        yield 'lino_xl.lib.addresses'
        yield 'lino_xl.lib.phones'
        yield 'lino_mentori.lib.contacts'
        yield 'lino_mentori.lib.mentori'
        yield 'lino_mentori.lib.courses'  # pupil__gender
        # yield 'lino_xl.lib.courses'
        # yield 'lino_xl.lib.rooms'

        # yield 'lino.modlib.comments'
        # yield 'lino.modlib.notify'
        # yield 'lino_xl.lib.uploads'
        yield 'lino.modlib.uploads'
        # yield 'lino.modlib.dupable'
        yield 'lino_xl.lib.lists'
        # yield 'lino_xl.lib.notes'
        # yield 'lino_xl.lib.beid'
        yield 'lino_xl.lib.cv'
        yield 'lino_xl.lib.trends'
        yield 'lino_xl.lib.polls'

        yield 'lino.modlib.checkdata'
        yield 'lino.modlib.export_excel'
        yield 'lino.modlib.weasyprint'
        yield 'lino_xl.lib.excerpts'
        yield 'lino.modlib.dashboard'
        yield 'lino_xl.lib.appypod'

    def get_plugin_configs(self):
        yield super(Site, self).get_plugin_configs()
        yield ('cv', 'with_language_history', True)
        yield ('cv', 'person_model', 'contacts.Person')
        yield ('trends', 'subject_model', 'contacts.Person')
        # yield ('uploads', 'expiring_start', -30)
        # yield ('uploads', 'expiring_end', 365)
        yield ('weasyprint', 'header_height', 30)

    def setup_quicklinks(self, user, tb):
        super(Site, self).setup_quicklinks(user, tb)
        tb.add_action('contacts.Persons')
        # a = self.models.users.MySettings.default_action
        # tb.add_instance_action(
        #     user, action=a, label=_("My settings"))


# the following line should not be active in a checked-in version
# DATABASES['default']['NAME'] = ':memory:'

USE_TZ = True
TIME_ZONE = 'UTC'
