# -*- coding: UTF-8 -*-
# Copyright 2017-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.api import dd, rt, _
from lino.utils import join_words
from lino.mixins import Hierarchical, Referrable

from lino_xl.lib.contacts.models import *

# Partner.define_action(show_problems=dd.ShowSlaveTable(
#     'checkdata.MessagesByOwner', icon_name='bell', combo_group="checkdata"))


class PartnerDetail(PartnerDetail):

    main = """
    overview #address_box:60 contact_box:30
    bottom_box
    """

    # address_box = dd.Panel("""
    # name_box
    # country #region city zip_code:10
    # addr1
    # #street_prefix street:25 street_no street_box
    # #addr2
    # """)  # , label=_("Address"))

    contact_box = dd.Panel("""
    name id
    url
    phone
    gsm #fax
    """,
                           label=_("Contact"))

    bottom_box = """
    remarks lists.MembersByPartner
    """


Partners.detail_layout = 'contacts.PartnerDetail'


class Person(Person, Referrable):

    class Meta(Person.Meta):
        abstract = dd.is_abstract_model(__name__, 'Person')

    @dd.virtualfield(dd.ForeignKey('courses.Course'))
    def active_course(self, ar):
        if self.pk is None: return
        qs = rt.models.courses.Enrolment.objects.filter(pupil=self)
        qs = qs.filter(start_date__lte=dd.today())
        qs = qs.filter(end_date__isnull=True)
        e = qs.first()
        if e is not None:
            return e.course

    @classmethod
    def setup_parameters(cls, fields):
        fields.setdefault(
            'company', dd.ForeignKey('contacts.Company', blank=True,
                                     null=True))
        fields.setdefault(
            'function',
            dd.ForeignKey('contacts.RoleType', blank=True, null=True))
        fields.setdefault('topic',
                          dd.ForeignKey('topics.Topic', blank=True, null=True))
        super(Person, cls).setup_parameters(fields)

    @classmethod
    def get_simple_parameters(cls):
        lst = list(super(Person, cls).get_simple_parameters())
        lst.append('company')
        lst.append('function')
        lst.append('topic')
        return lst

    @classmethod
    def add_param_filter(cls,
                         qs,
                         lookup_prefix='',
                         company=None,
                         function=None,
                         topic=None,
                         **kwargs):
        qs = super(Person, cls).add_param_filter(qs, **kwargs)
        if company:
            fkw = dict()
            wanted = company.whole_clan()
            fkw[lookup_prefix + 'rolesbyperson__company__in'] = wanted
            qs = qs.filter(**fkw)

        if function:
            fkw = dict()
            fkw[lookup_prefix + 'rolesbyperson__type'] = function
            qs = qs.filter(**fkw)

        if topic:
            fkw = dict()
            wanted = topic.whole_clan()
            fkw[lookup_prefix + 'interest_set__topic__in'] = wanted
        return qs

    # @classmethod
    # def get_request_queryset(cls, ar):
    #     qs = super(Person, cls).get_request_queryset(ar)
    #     pv = ar.param_values
    #     if pv.skill:
    #     return qs


# We use the `overview` field only in detail forms, and we
# don't want it to have a label "Description":
dd.update_field(Person, 'overview', verbose_name=None)
dd.update_field(Person, 'ref', verbose_name=_("Registration number"))

# class Pupil(Person):
#     class Meta(Company.Meta):
#         abstract = dd.is_abstract_model(__name__, 'Pupil')
#         verbose_name = _("Student")
#         verbose_name_plural = _("Students")
#
# class Teacher(Person):
#     class Meta(Company.Meta):
#         abstract = dd.is_abstract_model(__name__, 'Teacher')
#         verbose_name = _("Teacher")
#         verbose_name_plural = _("Teachers")
#


class Company(Company, Hierarchical):

    class Meta(Company.Meta):
        abstract = dd.is_abstract_model(__name__, 'Company')

    # def get_overview_elems(self, ar):
    #     elems = super(Company, self).get_overview_elems(ar)
    #     # elems += AddressOwner.get_overview_elems(self, ar)
    #     elems += ContactDetailsOwner.get_overview_elems(self, ar)
    #     return elems


class PersonDetail(PersonDetail):

    main = "general #contact career pupil teacher more"

    general = dd.Panel("""
    overview contact_box #phones.ContactDetailsByPartner
    contacts.RolesByPerson:30 lists.MembersByPartner:30
    """,
                       label=_("General"))

    contact_box = dd.Panel("""
    last_name first_name:15
    gender title:10 language:10
    birth_date age:10 ref:6
    """)  #, label=_("Contact"))

    # contact = dd.Panel("""
    # address_box
    # remarks
    # """, label=_("Contact"))

    career = dd.Panel("""
    career_box:50 trends.EventsBySubject:30
    """,
                      label=_("Career"))

    career_box = """
    cv.StudiesByPerson
    # cv.TrainingsByPerson
    cv.ExperiencesByPerson:40
    """

    pupil = dd.Panel("""
    active_course
    courses.InternshipEnrolmentsByPupil
    """,
                     label=_("Pupil"))

    teacher = dd.Panel("""
    courses.ActivitiesByTeacher
    courses.SeminarEnrolmentsByPupil
    """,
                       label=_("Teacher"))

    more = dd.Panel("""
    more_left #comments.CommentsByRFC:30
    courses.EnrolmentsByPupil
    """,
                    label=_("More"))

    more_left = """
    remarks id
    checkdata.MessagesByOwner:20 polls.ResponsesByPartner
    uploads.UploadsByController:20
    """


class CompaniesByCompany(Companies):
    label = _("Child organisations")
    master_key = 'parent'
    column_names = 'name_column email id *'


class CompanyDetail(CompanyDetail):
    main = "general more"

    general = dd.Panel("""
    overview:50 data_box:30 #address_box
    contacts.RolesByCompany:30 lists.MembersByPartner:30 uploads.UploadsByController:20
    """,
                       label=_("General"))

    data_box = """
    name id:6
    language:10
    # parent
    type:20
    """
    more = dd.Panel("""
    CompaniesByCompany checkdata.MessagesByOwner
    remarks #comments.CommentsByRFC
    """,
                    label=_("More"))


# @dd.receiver(dd.post_analyze)
# def my_details(sender, **kw):
#     contacts = sender.models.contacts
#     contacts.Companies.set_detail_layout(contacts.CompanyDetail())

# Companies.set_detail_layout(CompanyDetail())
# Persons.set_detail_layout(PersonDetail())
Person.column_names = 'last_name first_name gsm email city *'
Persons.params_layout = 'observed_event start_date end_date company function topic'
Persons.insert_layout = """
first_name last_name
gender country language
email
gsm
"""
Persons.react_big_search = True
Persons.column_names = 'last_name first_name *'
