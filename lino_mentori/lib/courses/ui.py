# Copyright 2017-2020 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from decimal import Decimal
from lino_xl.lib.courses.ui import *
from lino.api import _

from lino.core.gfks import gfk2lookup
from lino.utils import join_elems
from lino.utils.html import E
from lino.modlib.users.mixins import My
from lino_xl.lib.coachings.roles import CoachingsUser

AllActivities.column_names = "line:20 start_date:8 teacher user " \
                             "weekdays_text:10 times_text:10"

AllEnrolments.column_names = "id request_date start_date end_date \
user course pupil pupil__birth_date pupil__age pupil__country \
pupil__city pupil__gender"


class EnrolmentsByCourse(EnrolmentsByCourse):
    column_names = 'id request_date pupil pupil__gender ' \
                   'needs_childcare needs_bus '\
                   'remark workflow_buttons *'


Enrolments.detail_layout = """
request_date user start_date end_date
course pupil
needs_childcare needs_bus
remark:40 workflow_buttons:40 printed:20
confirmation_details
"""


class ActivityPlanning(Activities):
    required_roles = dd.login_required(CoursesUser)
    label = _("Activity planning")
    column_names = \
        "detail_link state "\
        "max_places requested confirmed trying free_places " \
        "childcare_needed bus_needed *"


class EnrolmentsBySeminar(EnrolmentsByCourse):
    column_names = 'request_date pupil places:8 remark workflow_buttons *'
    insert_layout = """
    pupil
    places
    remark
    request_date user
    """


class EnrolmentsByInternship(EnrolmentsByCourse):
    column_names = 'request_date pupil places:8 remark workflow_buttons *'
    insert_layout = """
    pupil
    places
    remark
    request_date user
    """


class CourseDetail(CourseDetail):
    main = "general #cal_tab enrolments"

    general = dd.Panel("""
    line teacher start_date end_date
    room workflow_buttons id:8 user
    name
    description
    """,
                       label=_("General"))


class SeminarDetail(CourseDetail):
    enrolments = dd.Panel("""
    enrolments_top
    EnrolmentsBySeminar
    """,
                          label=_("Enrolments"))


class Seminars(ActivitiesByLayout):
    activity_layout = 'seminars'
    detail_layout = 'courses.SeminarDetail'
    column_names = "name start_date enrolments_until line " \
                   "workflow_buttons *"


class SeminarEnrolmentsByPupil(EnrolmentsByPupil):
    activity_layout = 'seminars'


class InternshipDetail(CourseDetail):
    enrolments = dd.Panel("""
    enrolments_top
    EnrolmentsByInternship
    """,
                          label=_("Enrolments"))


class Internships(ActivitiesByLayout):
    activity_layout = 'internships'
    detail_layout = 'courses.SeminarDetail'
    column_names = "name start_date enrolments_until line " \
                   "workflow_buttons *"


class InternshipEnrolmentsByPupil(EnrolmentsByPupil):
    activity_layout = 'internships'
