# -*- coding: UTF-8 -*-
# Copyright 2017-2020 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from decimal import Decimal

from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy as pgettext

from lino.utils.html import E, join_elems
from lino.core.gfks import gfk2lookup

from lino_xl.lib.courses.models import *

from lino.modlib.users.mixins import UserAuthored
from lino_xl.lib.courses.roles import CoursesUser
from lino_xl.lib.excerpts.mixins import Certifiable
from lino_xl.lib.accounting.utils import ZERO, myround
# from lino.modlib.checkdata.choicelists import Checker
# from lino.modlib.summaries.mixins import SimpleSummary

# contacts = dd.resolve_app('contacts')
"""The default activity are **internships**.  A **seminar**

"""
ActivityLayouts.clear()
add = ActivityLayouts.add_item
add('C', _("Classes"), 'default')
add('S', _("Seminars"), 'seminars', 'courses.Seminars')
add('I', _("Interships"), 'internships', 'courses.Internships')


class Course(Course):

    class Meta(Course.Meta):
        # app_label = 'courses'
        abstract = dd.is_abstract_model(__name__, 'Course')
        # verbose_name = _("Course")
        # verbose_name_plural = _('Courses')

    @dd.virtualfield(models.IntegerField(_("Bus")))
    def bus_needed(self, ar):
        return self.get_places_sum(state=EnrolmentStates.requested,
                                   needs_bus=True)

    @dd.virtualfield(models.IntegerField(_("Childcare")))
    def childcare_needed(self, ar):
        return self.get_places_sum(state=EnrolmentStates.requested,
                                   needs_childcare=True)


class Enrolment(Enrolment):

    class Meta(Enrolment.Meta):
        abstract = dd.is_abstract_model(__name__, 'Enrolment')

    needs_childcare = models.BooleanField(_("Childcare"), default=False)
    needs_bus = models.BooleanField(_("Bus"), default=False)

    @dd.virtualfield(dd.HtmlBox(_("Participant")))
    def pupil_info(self, ar):
        txt = self.pupil.get_full_name(nominative=True)
        if ar is None:
            elems = [txt]
        else:
            elems = [ar.obj2html(self.pupil, txt)]
        # elems += [', ']
        # elems += join_elems(
        #     list(self.pupil.address_location_lines()),
        #     sep=', ')
        return E.p(*elems)

    # @classmethod
    # def setup_parameters(cls, fields):
    #     fields.update(
    #         coached_by=dd.ForeignKey(
    #             settings.SITE.user_model, verbose_name=_("Coached by"),
    #             blank=True))
    #     super(Enrolment, cls).setup_parameters(fields)

    # def disabled_fields(self, ar):
    #     rv = super(Enrolment, self).disabled_fields(ar)
    #     if not ar.get_user().user_type.has_required_roles([ClientsUser]):
    #         rv.add("pupil")
    #     return rv


from .ui import *
