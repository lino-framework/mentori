from atelier.invlib import setup_from_tasks

ns = setup_from_tasks(
    globals(),
    "lino_mentori",
    languages="en de fr el".split(),
    # tolerate_sphinx_warnings=True,
    locale_dir='lino_mentori/lib/mentori/locale',
    revision_control_system='git',
    cleanable_files=['docs/api/lino_mentori.*'],
    demo_projects=['lino_mentori.projects.mentori1'])
