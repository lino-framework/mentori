import datetime
from atelier.sphinxconf import configure

configure(globals())
from lino.sphinxcontrib import configure

configure(globals(), 'lino_mentori.projects.mentori1.settings.demo')

extensions += ['lino.sphinxcontrib.help_texts_extractor']
help_texts_builder_targets = {'lino_mentori.': 'lino_mentori.lib.mentori'}

project = "Lino Mentori"
copyright = '2020-{} Rumma & Ko Ltd'.format(datetime.date.today().year)
html_title = "Lino Mentori"

# html_context.update(public_url='https://lino-framework.gitlab.io/mentori')
