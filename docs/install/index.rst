.. _mentori.install:

=======================
Installing Lino Mentori
=======================

- Install Lino (the framework) as documented in the
  `Lino Developer Guide <https://www.lino-framework.org/dev/index.html>`__

- Go to your :xfile:`repositories` directory and download also a copy
  of the *Lino Mentori* repository::

    cd ~/repositories
    git clone https://gitlab.com/lino-framework/mentori.git

- Use pip to install this as editable package::

    pip install -e mentori

- Create a local Lino project as explained in
  `Your first local Lino project <https://www.lino-framework.org/dev/hello/index.html>`__.

- Change your project's :xfile:`settings.py` file so that it looks as
  follows:

  .. literalinclude:: settings.py
