.. _mentori.specs:

===============
Technical specs
===============

This section contains articles which are meant as technical
specifications. One of their goal is to get tested.


.. toctree::
   :maxdepth: 1
   :glob:

   general
   courses
   db
   roles
