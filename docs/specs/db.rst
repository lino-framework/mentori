.. doctest docs/specs/db.rst
.. _mentori.specs.db:

==================================
Database structure of Lino Mentori
==================================

This document describes the database structure.

.. contents::
  :local:

.. include:: /../docs/shared/include/tested.rst

>>> import lino
>>> lino.startup('lino_mentori.projects.mentori1.settings.doctests')
>>> from lino.api.doctest import *


>>> analyzer.show_db_overview()
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
32 plugins: lino, about, jinja, react, printing, system, users, office, xl, countries, contenttypes, gfks, linod, checkdata, addresses, phones, contacts, mentori, courses, uploads, lists, languages, cv, trends, polls, export_excel, weasyprint, excerpts, dashboard, appypod, staticfiles, sessions.
53 models:
========================== ======================= ========= =======
 Name                       Default table           #fields   #rows
-------------------------- ----------------------- --------- -------
 addresses.Address          addresses.Addresses     16        108
 checkdata.Message          checkdata.Messages      6         0
 contacts.Company           contacts.Companies      23        12
 contacts.CompanyType       contacts.CompanyTypes   7         16
 contacts.Partner           contacts.Partners       20        81
 contacts.Person            contacts.Persons        28        69
 contacts.Role              contacts.Roles          4         3
 contacts.RoleType          contacts.RoleTypes      5         5
 contenttypes.ContentType   gfks.ContentTypes       3         53
 countries.Country          countries.Countries     6         10
 countries.Place            countries.Places        9         80
 courses.Course             courses.Activities      29        40
 courses.Enrolment          courses.Enrolments      15        296
 courses.Line               courses.Lines           19        5
 courses.Slot               courses.Slots           5         0
 courses.Topic              courses.Topics          4         3
 cv.Duration                cv.Durations            4         5
 cv.EducationLevel          cv.EducationLevels      7         5
 cv.Experience              cv.Experiences          18        0
 cv.Function                cv.Functions            6         0
 cv.LanguageKnowledge       cv.LanguageKnowledges   11        0
 cv.Regime                  cv.Regimes              4         3
 cv.Sector                  cv.Sectors              5         0
 cv.Status                  cv.Statuses             4         7
 cv.Study                   cv.Studies              15        0
 cv.StudyType               cv.StudyTypes           7         11
 cv.Training                cv.Trainings            17        0
 dashboard.Widget           dashboard.Widgets       5         0
 excerpts.Excerpt           excerpts.Excerpts       11        0
 excerpts.ExcerptType       excerpts.ExcerptTypes   17        2
 languages.Language         languages.Languages     5         5
 linod.SystemTask           linod.SystemTasks       24        1
 lists.List                 lists.Lists             7         8
 lists.ListType             lists.ListTypes         4         3
 lists.Member               lists.Members           5         81
 phones.ContactDetail       phones.ContactDetails   8         5
 polls.AnswerChoice         polls.AnswerChoices     4         0
 polls.AnswerRemark         polls.AnswerRemarks     4         0
 polls.Choice               polls.Choices           6         35
 polls.ChoiceSet            polls.ChoiceSets        4         8
 polls.Poll                 polls.Polls             11        1
 polls.Question             polls.Questions         9         23
 polls.Response             polls.Responses         7         1
 sessions.Session           users.Sessions          3         ...
 system.SiteConfig          system.SiteConfigs      5         1
 trends.TrendArea           trends.TrendAreas       4         0
 trends.TrendEvent          trends.TrendEvents      7         0
 trends.TrendStage          trends.TrendStages      7         0
 uploads.Upload             uploads.Uploads         12        0
 uploads.UploadType         uploads.UploadTypes     8         0
 uploads.Volume             uploads.Volumes         5         0
 users.Authority            users.Authorities       3         0
 users.User                 users.AllUsers          21        3
========================== ======================= ========= =======
<BLANKLINE>
