.. doctest docs/specs/roles.rst
.. _mentori.specs.roles:

==========================
User roles in Lino Mentori
==========================

>>> import lino
>>> lino.startup('lino_mentori.projects.mentori1.settings.doctests')
>>> from lino.api.doctest import *

Menus
-----

Site manager
------------------

Robin is a :term:`site manager`, he has a complete menu.

>>> ses = rt.login('robin')
>>> ses.user.user_type
<users.UserTypes.admin:900>

>>> show_menu('robin')
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
- Contacts : Persons, Organizations, Partner Lists
- Activities : My Activities, Classes, Seminars, Interships, -, Activity lines, Pending requested enrolments, Pending confirmed enrolments, Activity planning
- Office : My Upload files, My Excerpts, Data problem messages assigned to me
- Polls : My Polls, My Responses
- Configure :
  - System : Users, Site Parameters, System tasks
  - Places : Countries, Places
  - Contacts : Legal forms, Functions, List Types
  - Activities : Topics, Timetable Slots
  - Office : Library volumes, Upload types, Excerpt Types
  - Career : Education Types, Education Levels, Activity sectors, Job titles, Work Regimes, Statuses, Contract Durations, Languages
  - Trends : Trend areas, Trend stages
  - Polls : Choice Sets
- Explorer :
  - System : Authorities, User types, User roles, All dashboard widgets, content types, Background procedures, Data checkers, Data problem messages
  - Contacts : Address types, Addresses, Contact detail types, Contact details, Contact persons, Partners, List memberships
  - Activities : Activities, Enrolments, Enrolment states, Course layouts, Activity states
  - Office : Upload files, Upload areas, Excerpts
  - Career : Language knowledges, Trainings, Studies, Job Experiences
  - Trends : Trend events
  - Polls : Polls, Questions, Choices, Responses, Answer Choices, Answer Remarks
- Site : About, User sessions
