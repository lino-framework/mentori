.. _mentori.specs.courses:

=======================
Courses in Lino Mentori
=======================


.. contents::
  :local:


.. include:: /../docs/shared/include/tested.rst

>>> import lino
>>> lino.startup('lino_mentori.projects.mentori1.settings.doctests')
>>> from lino.api.doctest import *


>>> rt.show(courses.ActivityLayouts)
... #doctest: +NORMALIZE_WHITESPACE -REPORT_UDIFF
======= ============= ============ ============================
 value   name          text         Table
------- ------------- ------------ ----------------------------
 C       default       Classes      courses.ActivitiesByLayout
 S       seminars      Seminars     courses.Seminars
 I       internships   Interships   courses.Internships
======= ============= ============ ============================
<BLANKLINE>
