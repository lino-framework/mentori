.. _mentori.specs.general:

================================
General overview of Lino Mentori
================================

The goal of Lino Mentori is

.. contents::
  :local:


.. include:: /../docs/shared/include/tested.rst

>>> import lino
>>> lino.startup('lino_mentori.projects.mentori1.settings.doctests')
>>> from lino.api.doctest import *


Show the list of members:

>>> rt.show(rt.models.users.AllUsers)
... #doctest: +NORMALIZE_WHITESPACE -REPORT_UDIFF
========== ===================== ============ ===========
 Username   User type             First name   Last name
---------- --------------------- ------------ -----------
 robin      900 (Administrator)   Robin        Rood
 rolf       900 (Administrator)   Rolf         Rompen
 romain     900 (Administrator)   Romain       Raffault
========== ===================== ============ ===========
<BLANKLINE>
