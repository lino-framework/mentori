============================
The ``lino-mentori`` package
============================





A Lino application for managing the students of a school and their internships
and mentors in different organizations.

Project homepage: https://gitlab.com/lino-framework/mentori

Documentation: https://lino-framework.gitlab.io/mentori/


